#include "borast/borast-traps-private.h"

borast_status_t bo_poly_to_traps (hidGC gc, POLYAREA *poly, borast_traps_t *traps);
borast_status_t bo_contour_to_traps (hidGC gc, PLINE *contour, borast_traps_t *traps);
